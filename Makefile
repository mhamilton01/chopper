CC = g++
CFLAGS = -g -I. -I./include -pthread

LIBS = -lEposCmd

all: chopper

OBJ = chopper.o server.o vcs_api.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

%.o: %.cpp $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

chopper: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -f *.o *~ core chopper

