#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 
#include <thread>
#include <string>
#include <iostream>
#include <iomanip>
#include <errno.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "vcs_api.h" // must be after rapidjson includes due to namespace issues

#define PORT 7735
bool running = false;
char buffer[1024];

char *ack2json(char *b);
ACK* json2cmd(char *buffer);

void getMsg() {
  struct sockaddr_in addr;
  struct sockaddr_in cli;
  int sockfd;
  socklen_t addrlen = sizeof(addr);
  socklen_t clilen = sizeof(cli);
  int opt = 1;
  int newsockfd;
  bool looping = true;

  if ((sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == 0) {
    cout << "socket failure" << endl;
    exit(EXIT_FAILURE);
  }
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
    cout << "setsockopt failure" << endl;
    exit(EXIT_FAILURE);
  }
  bzero((char *) &addr, addrlen);
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = INADDR_ANY;
  addr.sin_port = htons(PORT);
  if (bind(sockfd, (struct sockaddr *) &addr, addrlen) < 0) {
    cout << "bind failure" << endl;
    exit(EXIT_FAILURE);
  }
  while (looping) {
    if (listen(sockfd, 3) < 0) {
      cout << "listen failure" << endl;
      exit(EXIT_FAILURE);
    }
    //cout << "waiting for cmd" << endl;
    if ((newsockfd = accept(sockfd, (struct sockaddr *) &cli, &clilen)) < 0) {
      cout << "accept failure newsockfd=" << newsockfd << " sockfd=" << sockfd << endl;
      cout << "error " << errno << " " << strerror(errno) << endl;
      exit(EXIT_FAILURE);
    }
    //cout << "waiting for read" << endl;
    bzero(buffer, sizeof(buffer));
    int cnt = recv(newsockfd, buffer, 2, MSG_WAITALL);
    int size = buffer[0] | (buffer[1] &  0xff);
    //cout << "recvsize " << size << " bytes " << cnt << endl;
    cnt = recv(newsockfd, buffer, size, MSG_WAITALL);
    //cout << "recv " << cnt << " bytes" << endl;
    int str_len = strlen(buffer);
    ACK* ack = json2cmd(buffer);

    if (ack != 0) {
      rapidjson::Document d;
      rapidjson::Document::AllocatorType& alloc = d.GetAllocator();
      d.SetObject();
  
      rapidjson::Value stat;
      rapidjson::Value erro;
      rapidjson::Value resp1;
      rapidjson::Value resp2;
      rapidjson::Value resp3;
      rapidjson::Value resp4;
      rapidjson::Value resp5;
      rapidjson::Value resp6;
      rapidjson::Value resp7;
      rapidjson::Value resp8;
      rapidjson::Value resp9;
      rapidjson::Value resp10;
      stat.SetString(ack->status.c_str(), alloc);
      erro.SetString(ack->error.c_str(), alloc);
      resp1.SetString(ack->response1.c_str(), alloc);
      resp2.SetString(ack->response2.c_str(), alloc);
      resp3.SetString(ack->response3.c_str(), alloc);
      resp4.SetString(ack->response4.c_str(), alloc);
      resp5.SetString(ack->response5.c_str(), alloc);
      resp6.SetString(ack->response6.c_str(), alloc);
      resp7.SetString(ack->response7.c_str(), alloc);
      resp8.SetString(ack->response8.c_str(), alloc);
      resp9.SetString(ack->response9.c_str(), alloc);
      resp10.SetString(ack->response10.c_str(), alloc);
      d.AddMember("status", stat, alloc);
      d.AddMember("error", erro, alloc);
      d.AddMember("response1", resp1, alloc);
      d.AddMember("response2", resp2, alloc);
      d.AddMember("response3", resp3, alloc);
      d.AddMember("response4", resp4, alloc);
      d.AddMember("response5", resp5, alloc);
      d.AddMember("response6", resp6, alloc);
      d.AddMember("response7", resp7, alloc);
      d.AddMember("response8", resp8, alloc);
      d.AddMember("response9", resp9, alloc);
      d.AddMember("response10", resp10, alloc);

      rapidjson::StringBuffer sb;
      rapidjson::Writer<rapidjson::StringBuffer> writer(sb);
      d.Accept(writer);

      string ss = sb.GetString();
      int len = ss.length();
      char outbuf[len+1];
      strcpy(outbuf, ss.c_str());

      send(newsockfd, &len, sizeof(len), 0);
      send(newsockfd, outbuf, len, 0);
    }
    close(newsockfd);
  }  
}

ACK* json2cmd(char *b) {
  if (strlen(b) < 10) {
    return 0;
  }
  //cout << "json2cmd char *b = " << b << " size " << strlen(b) << endl;
  rapidjson::Document d;
  d.Parse(b);
  rapidjson::Value& name = d["name"];
  string rname = name.GetString();
  rapidjson::Value& arg1 = d["arg1"];
  string rarg1 = arg1.GetString();
  rapidjson::Value& arg2 = d["arg2"];
  string rarg2 = arg2.GetString();
  rapidjson::Value& arg3 = d["arg3"];
  string rarg3 = arg3.GetString();
  rapidjson::Value& arg4 = d["arg4"];
  string rarg4 = arg4.GetString();
  rapidjson::Value& arg5 = d["arg5"];
  string rarg5 = arg5.GetString();
  rapidjson::Value& arg6 = d["arg6"];
  string rarg6 = arg6.GetString();
  rapidjson::Value& arg7 = d["arg7"];
  string rarg7 = arg7.GetString();
  rapidjson::Value& arg8 = d["arg8"];
  string rarg8 = arg8.GetString();
  rapidjson::Value& arg9 = d["arg9"];
  string rarg9 = arg9.GetString();
  rapidjson::Value& arg10 = d["arg10"];
  string rarg10 = arg10.GetString();

  // replace rid with the cmd id from a lookup table
  ACK* ack = execCommand(rname, rarg1, rarg2, rarg3, rarg4, rarg5, rarg6, rarg7, rarg8, rarg9, rarg10);
  /*
  cout << "     ack.status " << ack->status;
  if (ack->response1 != "-1") {
    if (ack->response1 != "") {
      cout << " ack.response1 " << ack->response1;
    }
  }
  if (ack->response2 != "-1") {
    if (ack->response2 != "") {
      cout << " ack.response2 " << ack->response2;
    }
  }
  cout << endl;
  if (ack->status != "1") {
    unsigned int e = std::atoi(ack->error.c_str());
    cout << "     error 0x" << std::setfill('0') << std::setw(sizeof(unsigned int))
	 << std::hex << e << endl;
  }
  */
  return ack;
}

void startServer() {
  thread th1(getMsg);
  th1.detach();
  //getMsg();
}

void killServer() {
  std::terminate();
}
