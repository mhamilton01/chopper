#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include "Definitions.h"
#include "vcs_api.h"

using namespace std;

#ifndef MMC_SUCCESS
#define MMC_SUCCESS 0
#endif

#ifndef MMC_FAILED
#define MMC_FAILED 1
#endif

ACK* execCommand(string str, string a1, string a2, string a3, string a4, string a5, string a6, string a7, string a8, string a9, string a10) {
  unsigned int error = 0;
  int returnStatus = -1;
  ACK *ack = new ACK();
  int node = std::atoi(a2.c_str());
  void *handle = (void *) std::atoi(a1.c_str());

  //ack->response1 = "";
  
  cout << "execCommand " << str << " a1 \"" << a1
       << "\" a2 \"" << a2 << "\" a3 \"" << a3 << "\" a4 \"" << a4
       << "\" a5 \"" << a5 << "\" a6 \"" << a6 << "\" a7 \"" << a7
       << "\" a8 \"" << a8 << "\" a9 \"" << a9 << "\" a9 \"" << a9
       << "\" a10 \"" << a10
       << "\"" << endl;
  /**/
  if (str.compare("VCS_ActivateAnalogCurrentSetpoint") == 0) {
    returnStatus = VCS_ActivateAnalogCurrentSetpoint(handle,
						     (int) std::atoi(a2.c_str()),
						     (short) std::atoi(a3.c_str()),
						     (float) std::atof(a4.c_str()),
						     (short) std::atoi(a5.c_str()),
							   &error);
  } else if (str.compare("VCS_ActivateAnalogPositionSetpoint") == 0) {
    //VCS_ActivateAnalogPositionSetpoint();
  } else if (str.compare("VCS_ActivateAnalogVelocitySetpoint") == 0) {
    //VCS_ActivateAnalogVelocitySetpoint();
  } else if (str.compare("VCS_ActivateCurrentMode") == 0) {
    //VCS_ActivateCurrentMode();
  } else if (str.compare("VCS_ActivateHomingMode") == 0) {
    returnStatus = VCS_ActivateHomingMode(handle, node, &error);
  } else if (str.compare("VCS_ActivateInterpolatedPositionMode") == 0) {
    //VCS_ActivateInterpolatedPositionMode();
  } else if (str.compare("VCS_ActivateMasterEncoderMode") == 0) {
    //VCS_ActivateMasterEncoderMode();
  } else if (str.compare("VCS_ActivatePositionCompare") == 0) {
    //VCS_ActivatePositionCompare();
  } else if (str.compare("VCS_ActivatePositionMarker") == 0) {
    //VCS_ActivatePositionMarker();
  } else if (str.compare("VCS_ActivatePositionMode") == 0) {
    //VCS_ActivatePositionMode();
  } else if (str.compare("VCS_ActivateProfilePositionMode") == 0) {
    returnStatus = VCS_ActivateProfilePositionMode(handle, node, &error);
  } else if (str.compare("VCS_ActivateProfileVelocityMode") == 0) {
    returnStatus = VCS_ActivateProfileVelocityMode(handle, node, &error);
  } else if (str.compare("VCS_ActivateStepDirectionMode") == 0) {
    //VCS_ActivateStepDirectionMode();
  } else if (str.compare("VCS_ActivateVelocityMode") == 0) {
    //VCS_ActivateVelocityMode();
  } else if (str.compare("VCS_AddPvtValueToIpmBuffer") == 0) {
    //VCS_AddPvtValueToIpmBuffer();
  } else if (str.compare("VCS_AnalogInputConfiguration") == 0) {
    //VCS_AnalogInputConfiguration();
  } else if (str.compare("VCS_AnalogOutputConfiguration") == 0) {
    //VCS_AnalogOutputConfiguration();
  } else if (str.compare("VCS_ClearFault") == 0) {
    returnStatus = VCS_ClearFault(handle, node, &error);
  } else if (str.compare("VCS_ClearIpmBuffer") == 0) {
    //VCS_ClearIpmBuffer();
  } else if (str.compare("VCS_CloseAllDevices") == 0) {
    //VCS_CloseAllDevices();
  } else if (str.compare("VCS_CloseAllSubDevices") == 0) {
    //VCS_CloseAllSubDevices();
  } else if (str.compare("VCS_CloseDevice") == 0) {
    returnStatus = VCS_CloseDevice(handle, &error);
    handle = 0;
  } else if (str.compare("VCS_CloseSubDevice") == 0) {
    //VCS_CloseSubDevice();
  } else if (str.compare("VCS_DeactivateAnalogCurrentSetpoint") == 0) {
    //VCS_DeactivateAnalogCurrentSetpoint();
  } else if (str.compare("VCS_DeactivateAnalogPositionSetpoint") == 0) {
    //VCS_DeactivateAnalogPositionSetpoint();
  } else if (str.compare("VCS_DeactivateAnalogVelocitySetpoint") == 0) {
    //VCS_DeactivateAnalogVelocitySetpoint();
  } else if (str.compare("VCS_DeactivatePositionCompare") == 0) {
    //VCS_DeactivatePositionCompare();
  } else if (str.compare("VCS_DeactivatePositionMarker") == 0) {
    //VCS_DeactivatePositionMarker();
  } else if (str.compare("VCS_DefinePosition") == 0) {
    //VCS_DefinePosition();
  } else if (str.compare("VCS_DigitalInputConfiguration") == 0) {
    int config;
    if (a4.compare("DIC_NO_FUNCTIONALITY") == 0) {
      //config = DIC_NO_FUNCTIONALITY;
      config = 255;
    } else if (a4.compare("DIC_GENERAL_PURPOSE_A") == 0) {
      config = DIC_GENERAL_PURPOSE_A;
    } else if (a4.compare("DIC_GENERAL_PURPOSE_B") == 0) {
      config = DIC_GENERAL_PURPOSE_B;
    } else if (a4.compare("DIC_GENERAL_PURPOSE_C") == 0) {
      config = DIC_GENERAL_PURPOSE_C;
    } else if (a4.compare("DIC_GENERAL_PURPOSE_D") == 0) {
      config = DIC_GENERAL_PURPOSE_D;
    } else if (a4.compare("DIC_GENERAL_PURPOSE_E") == 0) {
      config = DIC_GENERAL_PURPOSE_E;
    } else if (a4.compare("DIC_GENERAL_PURPOSE_F") == 0) {
      config = DIC_GENERAL_PURPOSE_F;
    } else if (a4.compare("DIC_GENERAL_PURPOSE_G") == 0) {
      config = DIC_GENERAL_PURPOSE_G;
    } else if (a4.compare("DIC_GENERAL_PURPOSE_H") == 0) {
      config = DIC_GENERAL_PURPOSE_H;
    } else if (a4.compare("DIC_GENERAL_PURPOSE_I") == 0) {
      config = DIC_GENERAL_PURPOSE_I;
    } else if (a4.compare("DIC_GENERAL_PURPOSE_J") == 0) {
      config = DIC_GENERAL_PURPOSE_J;
    } else if (a4.compare("DIC_QUICK_STOP") == 0) {
      config = DIC_QUICK_STOP;
    } else if (a4.compare("DIC_DRIVE_ENABLE") == 0) {
      config = DIC_DRIVE_ENABLE;
    } else if (a4.compare("DIC_POSITION_MARKER") == 0) {
      config = DIC_POSITION_MARKER;
    } else if (a4.compare("DIC_HOME_SWITCH") == 0) {
      config = DIC_HOME_SWITCH;
    } else if (a4.compare("DIC_POSITIVE_LIMIT_SWITCH") == 0) {
      config = DIC_POSITIVE_LIMIT_SWITCH;
    } else if (a4.compare("DIC_NEGATIVE_LIMIT_SWITCH") == 0) {
      config = DIC_NEGATIVE_LIMIT_SWITCH;
    } else {
      cout << "unknown configuration " << a4 << endl;
    }
    int mask;
    if (a5.compare("True") == 0) {
      mask = 1;
    } else if (a5.compare("False") == 0) {
      mask = 0;
    } else {
      cout << "unknown mask " << a5 << endl;
    }
    int polarity;
    if (a6.compare("ActiveLow") == 0) {
      polarity = 1;
    } else if (a6.compare("ActiveHigh") == 0) {
      polarity = 0;
    } else {
      cout << "unknown polarity " << a6 << endl;
    }
    int exmask;
    if (a5.compare("True") == 0) {
      exmask = 1;
    } else if (a5.compare("False") == 0) {
      exmask = 0;
    } else {
      cout << "unknown mask " << a5 << endl;
    }
    returnStatus = VCS_DigitalInputConfiguration(handle, node,
						 std::atoi(a3.c_str()),
						 config,
						 mask,
						 polarity,
						 exmask,
						 &error);
  } else if (str.compare("VCS_DigitalOutputConfiguration") == 0) {
    //VCS_DigitalOutputConfiguration();
  } else if (str.compare("VCS_DisableAnalogCurrentSetpoint") == 0) {
    //VCS_DisableAnalogCurrentSetpoint();
  } else if (str.compare("VCS_DisableAnalogPositionSetpoint") == 0) {
    //VCS_DisableAnalogPositionSetpoint();
  } else if (str.compare("VCS_DisableAnalogVelocitySetpoint") == 0) {
    //VCS_DisableAnalogVelocitySetpoint();
  } else if (str.compare("VCS_DisablePositionCompare") == 0) {
    //VCS_DisablePositionCompare();
  } else if (str.compare("VCS_DisablePositionWindow") == 0) {
    //VCS_DisablePositionWindow();
  } else if (str.compare("VCS_DisableVelocityWindow") == 0) {
    //VCS_DisableVelocityWindow();
  } else if (str.compare("VCS_EnableAnalogCurrentSetpoint") == 0) {
    //VCS_EnableAnalogCurrentSetpoint();
  } else if (str.compare("VCS_EnableAnalogPositionSetpoint") == 0) {
    //VCS_EnableAnalogPositionSetpoint();
  } else if (str.compare("VCS_EnableAnalogVelocitySetpoint") == 0) {
    //VCS_EnableAnalogVelocitySetpoint();
  } else if (str.compare("VCS_EnablePositionCompare") == 0) {
    //VCS_EnablePositionCompare();
  } else if (str.compare("VCS_EnablePositionWindow") == 0) {
    //VCS_EnablePositionWindow();
  } else if (str.compare("VCS_EnableVelocityWindow") == 0) {
    //VCS_EnableVelocityWindow();
  } else if (str.compare("VCS_FindHome") == 0) {
    int method;
    if (a3.compare("HM_ACTUAL_POSITION") == 0) {
      method = HM_ACTUAL_POSITION;
    } else if (a3.compare("HM_INDEX_POSITIVE_SPEED") == 0) {
      method = HM_INDEX_POSITIVE_SPEED;
    } else if (a3.compare("HM_INDEX_NEGATIVE_SPEED") == 0) {
      method = HM_INDEX_NEGATIVE_SPEED;
    } else if (a3.compare("HM_HOME_SWITCH_NEGATIVE_SPEED") == 0) {
      method = HM_HOME_SWITCH_NEGATIVE_SPEED;
    } else if (a3.compare("HM_HOME_SWITCH_POSITIVE_SPEED") == 0) {
      method = HM_HOME_SWITCH_POSITIVE_SPEED;
    } else if (a3.compare("HM_POSITIVE_LIMIT_SWITCH") == 0) {
      method = HM_POSITIVE_LIMIT_SWITCH;
    } else if (a3.compare("HM_NEGATIVE_LIMIT_SWITCH") == 0) {
      method = HM_NEGATIVE_LIMIT_SWITCH;
    } else if (a3.compare("HM_HOME_SWITCH_NEGATIVE_SPEED_AND_INDEX") == 0) {
      method = HM_HOME_SWITCH_NEGATIVE_SPEED_AND_INDEX;
    } else if (a3.compare("HM_HOME_SWITCH_POSITIVE_SPEED_AND_INDEX") == 0) {
      method = HM_HOME_SWITCH_POSITIVE_SPEED_AND_INDEX;
    } else if (a3.compare("HM_POSITIVE_LIMIT_SWITCH_AND_INDEX") == 0) {
      method = HM_POSITIVE_LIMIT_SWITCH_AND_INDEX;
    } else if (a3.compare("HM_NEGATIVE_LIMIT_SWITCH_AND_INDEX") == 0) {
      method = HM_NEGATIVE_LIMIT_SWITCH_AND_INDEX;
    } else if (a3.compare("HM_CURRENT_THRESHOLD_NEGATIVE_SPEED_AND_INDEX") == 0) {
      method = HM_CURRENT_THRESHOLD_NEGATIVE_SPEED_AND_INDEX;
    } else if (a3.compare("HM_CURRENT_THRESHOLD_POSITIVE_SPEED_AND_INDEX") == 0) {
      method = HM_CURRENT_THRESHOLD_POSITIVE_SPEED_AND_INDEX;
    } else if (a3.compare("HM_CURRENT_THRESHOLD_POSITIVE_SPEED") == 0) {
      method = HM_CURRENT_THRESHOLD_POSITIVE_SPEED;
    } else if (a3.compare("HM_CURRENT_THRESHOLD_NEGATIVE_SPEED") == 0) {
      method = HM_CURRENT_THRESHOLD_NEGATIVE_SPEED;
    } else {
      cout << "unknown homing method " << a3 << endl;
    }
    returnStatus = VCS_FindHome(handle, node, method, &error);
  } else if (str.compare("VCS_GetAllDigitalInputs") == 0) {
    //VCS_GetAllDigitalInputs();
  } else if (str.compare("VCS_GetAllDigitalOutputs") == 0) {
    //VCS_GetAllDigitalOutputs();
  } else if (str.compare("VCS_GetAnalogInput") == 0) {
    //VCS_GetAnalogInput();
  } else if (str.compare("VCS_GetAnalogInputState") == 0) {
    //VCS_GetAnalogInputState();
  } else if (str.compare("VCS_GetAnalogInputVoltage") == 0) {
    //VCS_GetAnalogInputVoltage();
  } else if (str.compare("VCS_GetBaudrateSelection") == 0) {
    //VCS_GetBaudrateSelection();
  } else if (str.compare("VCS_GetControllerGain") == 0) {
    unsigned long long value = 0;
    int regController;
    if (a3.compare("EC_PI_VELOCITY_CONTROLLER") == 0) {
      regController = EC_PI_VELOCITY_CONTROLLER;
    } else if (a3.compare("EC_PI_VELOCITY_CONTROLLER_WITH_OBSERVER") == 0) {
      regController = EC_PI_VELOCITY_CONTROLLER;
    } else {
      cout << "VCS_GetControllerGain arg2 unknown" << a3 << endl;
    }
    int regGain;
    if (a4.compare("EG_PIVC_P_GAIN") == 0) {
      regGain = EG_PIVC_P_GAIN;
    } else if (a4.compare("EG_PIVC_I_GAIN") == 0) {
      regGain = EG_PIVC_I_GAIN;
    } else if (a4.compare("EG_PIVC_FEED_FORWARD_VELOCITY_GAIN") == 0) {
      regGain = EG_PIVC_FEED_FORWARD_VELOCITY_GAIN;
    } else if (a4.compare("EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN") == 0) {
      regGain = EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN;
    } else if (a4.compare("EG_PIVCWO_P_GAIN") == 0) {
      regGain = EG_PIVCWO_P_GAIN;
    } else if (a4.compare("EG_PIVCWO_I_GAIN") == 0) {
      regGain = EG_PIVCWO_I_GAIN;
    } else if (a4.compare("EG_PIVCWO_FEED_FORWARD_VELOCITY_GAIN") == 0) {
      regGain = EG_PIVCWO_FEED_FORWARD_VELOCITY_GAIN;
    } else if (a4.compare("EG_PIVCWO_FEED_FORWARD_ACCELERATION_GAIN") == 0) {
      regGain = EG_PIVCWO_FEED_FORWARD_ACCELERATION_GAIN;
    } else if (a4.compare("EG_PIVCWO_OBSERVER_THETA_GAIN") == 0) {
      regGain = EG_PIVCWO_OBSERVER_THETA_GAIN;
    } else if (a4.compare("EG_PIVCWO_OBSERVER_OMEGA_GAIN") == 0) {
      regGain = EG_PIVCWO_OBSERVER_OMEGA_GAIN;
    } else if (a4.compare("EG_PIVCWO_OBSERVER_TAU_GAIN") == 0) {
      regGain = EG_PIVCWO_OBSERVER_TAU_GAIN;
    } else {
      cout << "VCS_GetControllerGain arg4 unknown" << a4 << endl;
    }
    returnStatus = VCS_GetControllerGain(handle, node,
					 regController, regGain, 
					 &value, &error);
    ack->response1 = std::to_string(value);
  } else if (str.compare("VCS_GetCurrentIs") == 0) {
    short cur;
    returnStatus = VCS_GetCurrentIs(handle, node, &cur, &error);
    ack->response1 = std::to_string(cur);
  } else if (str.compare("VCS_GetCurrentIsAveraged") == 0) {
    short cur;
    returnStatus = VCS_GetCurrentIsAveraged(handle, node, &cur, &error);
    ack->response1 = std::to_string(cur);
  } else if (str.compare("VCS_GetCurrentMust") == 0) {
    //VCS_GetCurrentMust();
  } else if (str.compare("VCS_GetCurrentRegulatorGain") == 0) {
    //VCS_GetCurrentRegulatorGain();
  } else if (str.compare("VCS_GetDcMotorParameter") == 0) {
    unsigned short nomCur = 0;
    unsigned short maxOutCur = 0;
    unsigned short thermalTimeConst = 0;
    returnStatus = VCS_GetDcMotorParameter(handle, node, &nomCur, &maxOutCur, & thermalTimeConst, &error);
    ack->response1 = std::to_string(nomCur);
    ack->response2 = std::to_string(maxOutCur);
    ack->response3 = std::to_string(thermalTimeConst);
  } else if (str.compare("VCS_GetDeviceErrorCode") == 0) {
    //VCS_GetDeviceErrorCode();
  } else if (str.compare("VCS_GetDeviceName") == 0) {
    //VCS_GetDeviceName();
  } else if (str.compare("VCS_GetDeviceNameSelection") == 0) {
    bool sos = a2.compare("true") ? true : false;
    unsigned short strSize = atoi(a3.c_str());
    char name[strSize];
    int end;
    returnStatus = VCS_GetDeviceNameSelection(sos, name, strSize, &end, &error);
    ack->response1.assign(name);
    ack->response2 = (end) ? "true" : "false";
  } else if (str.compare("VCS_GetDisableState") == 0) {
    //VCS_GetDisableState();
  } else if (str.compare("VCS_GetDriverInfo") == 0) {
    //VCS_GetDriverInfo();
  } else if (str.compare("VCS_GetEcMotorParameter") == 0) {
    unsigned short nomCur = 0;
    unsigned short maxOutCur = 0;
    unsigned short thermalTimeConst = 0;
    unsigned char polePairs = 0;
    VCS_GetEcMotorParameter(handle, node, &nomCur, &maxOutCur, &thermalTimeConst, &polePairs, &error);
    ack->response1 = std::to_string(nomCur);
    ack->response2 = std::to_string(maxOutCur);
    ack->response3 = std::to_string(thermalTimeConst);
    ack->response4 = std::to_string(polePairs);
  } else if (str.compare("VCS_GetEnableState") == 0) {
    int isEnabled;
    VCS_GetEnableState(handle, node, &isEnabled, &error);
    ack->response1 = std::to_string(isEnabled);
  } else if (str.compare("VCS_GetEncoderParameter") == 0) {
    //VCS_GetEncoderParameter();
  } else if (str.compare("VCS_GetErrorInfo") == 0) {
    //VCS_GetErrorInfo();
  } else if (str.compare("VCS_GetFaultState") == 0) {
    int fault;
    returnStatus = VCS_GetFaultState(handle, node, &fault, &error);
    ack->response1 = std::to_string(fault);
  } else if (str.compare("VCS_GetFreeIpmBufferSize") == 0) {
    //VCS_GetFreeIpmBufferSize();
  } else if (str.compare("VCS_GetGatewaySettings") == 0) {
    //VCS_GetGatewaySettings();
  } else if (str.compare("VCS_GetHallSensorParameter") == 0) {
    //VCS_GetHallSensorParameter();
  } else if (str.compare("VCS_GetHomingParameter") == 0) {
    unsigned int accel;
    unsigned int speedSwitch;
    unsigned int speedIndex;
    int offset;
    unsigned short threshold;
    int position;
    returnStatus = VCS_GetHomingParameter(handle, node, &accel, &speedSwitch,
					  &speedIndex, &offset, &threshold,
					  &position, &error);
    ack->response1 = std::to_string(accel);
    ack->response2 = std::to_string(speedSwitch);
    ack->response3 = std::to_string(speedIndex);
    ack->response4 = std::to_string(offset);
    ack->response5 = std::to_string(threshold);
    ack->response6 = std::to_string(position);
  } else if (str.compare("VCS_GetHomingState") == 0) {
    int homingAttained;
    int homingError;
    returnStatus = VCS_GetHomingState(handle, node, &homingAttained,
				      &homingError, &error);
    ack->response1 = homingAttained ? "True" : "False";
    ack->response2 = homingError ? "True" : "False";
  } else if (str.compare("VCS_GetIncEncoderParameter") == 0) {
    //VCS_GetIncEncoderParameter();
  } else if (str.compare("VCS_GetInterfaceName") == 0) {
    //VCS_GetInterfaceName();
  } else if (str.compare("VCS_GetInterfaceNameSelection") == 0) {
    char *name = (char *) a2.c_str();
    char *stackName = (char *) a3.c_str();
    bool sos = a4.compare("true") ? true : false;
    unsigned short strSize = atoi(a5.c_str());
    char ifaceName[strSize];
    int end;
    returnStatus = VCS_GetInterfaceNameSelection(name, stackName, sos, ifaceName, strSize, &end, &error);
    ack->response1.assign(ifaceName);
    ack->response2 = (end) ? "true" : "false";
  } else if (str.compare("VCS_GetIpmBufferParameter") == 0) {
    //VCS_GetIpmBufferParameter();
  } else if (str.compare("VCS_GetIpmReturnStatus") == 0) {
    //VCS_GetIpmReturnStatus();
  } else if (str.compare("VCS_GetKeyHandle") == 0) {
    //VCS_GetKeyHandle();
  } else if (str.compare("VCS_GetMasterEncoderParameter") == 0) {
    //VCS_GetMasterEncoderParameter();
  } else if (str.compare("VCS_GetMaxAcceleration") == 0) {
    unsigned int accel;
    returnStatus = VCS_GetMaxAcceleration(handle, node, &accel, &error);
    ack->response1 = std::to_string(accel);
  } else if (str.compare("VCS_GetMaxFollowingError") == 0) {
    //VCS_GetMaxFollowingError();
  } else if (str.compare("VCS_GetMaxProfileVelocity") == 0) {
    unsigned int vel;
    returnStatus = VCS_GetMaxProfileVelocity(handle, node, &vel, &error);
    ack->response1 = std::to_string(vel);
  } else if (str.compare("VCS_GetMotorParameter") == 0) {
    //VCS_GetMotorParameter();
  } else if (str.compare("VCS_GetMotorType") == 0) {
    unsigned short type = 0;
    returnStatus = VCS_GetMotorType(handle, node, &type, &error);
    ack->response1 = std::to_string(type);
  } else if (str.compare("VCS_GetMovementState") == 0) {
    //VCS_GetMovementState();
  } else if (str.compare("VCS_GetNbOfDeviceError") == 0) {
    //VCS_GetNbOfDeviceError();
  } else if (str.compare("VCS_GetObject") == 0) {
    //VCS_GetObject();
  } else if (str.compare("VCS_GetOperationMode") == 0) {
    //VCS_GetOperationMode();
  } else if (str.compare("VCS_GetPortName") == 0) {
    //VCS_GetPortName();
  } else if (str.compare("VCS_GetPortNameSelection") == 0) {
    char *name = (char *) a2.c_str();
    char *stackName = (char *) a3.c_str();
    char *iface = (char *) a4.c_str();
    bool sos = a5.compare("true") ? true : false;
    unsigned short strSize = atoi(a6.c_str());
    char portName[strSize];
    int end;
    returnStatus = VCS_GetPortNameSelection(name, stackName, iface, sos, portName, strSize, &end, &error);
    ack->response1.assign(portName);
    ack->response2 = (end) ? "true" : "false";
  } else if (str.compare("VCS_GetPositionCompareParameter") == 0) {
    //VCS_GetPositionCompareParameter();
  } else if (str.compare("VCS_GetPositionIs") == 0) {
    //VCS_GetPositionIs();
  } else if (str.compare("VCS_GetPositionMarkerParameter") == 0) {
    //VCS_GetPositionMarkerParameter();
  } else if (str.compare("VCS_GetPositionMust") == 0) {
    //VCS_GetPositionMust();
  } else if (str.compare("VCS_GetPositionProfile") == 0) {
    //VCS_GetPositionProfile();
  } else if (str.compare("VCS_GetPositionRegulatorFeedForward") == 0) {
    //VCS_GetPositionRegulatorFeedForward();
  } else if (str.compare("VCS_GetPositionRegulatorGain") == 0) {
    //VCS_GetPositionRegulatorGain();
  } else if (str.compare("VCS_GetProtocolStackName") == 0) {
    //returnStatus = VCS_GetProtocolStackName();
  } else if (str.compare("VCS_GetProtocolStackNameSelection") == 0) {
    char *name = (char *) a2.c_str();
    bool sos = a3.compare("true") ? true : false;
    unsigned short strSize = atoi(a4.c_str());
    char  stackName[strSize];
    int end;
    returnStatus = VCS_GetProtocolStackNameSelection(name, sos, stackName, strSize, &end, &error);
    ack->response1.assign(stackName);
    ack->response2 = (end) ? "true" : "false";
  } else if (str.compare("VCS_GetProtocolStackSettings") == 0) {
    unsigned int baudrate;
    unsigned int timeout;
    returnStatus = VCS_GetProtocolStackSettings(handle, &baudrate, &timeout, &error);
    ack->response1 = std::to_string(baudrate);
    ack->response2 = std::to_string(timeout);
  } else if (str.compare("VCS_GetQuickStopState") == 0) {
    //VCS_GetQuickStopState();
  } else if (str.compare("VCS_GetSensorType") == 0) {
    unsigned short st = 0;
    returnStatus = VCS_GetSensorType(handle, node, &st, &error);
    ack->response1 = std::to_string(st);
  } else if (str.compare("VCS_GetSsiAbsEncoderParameter") == 0) {
    //VCS_GetSsiAbsEncoderParameter();
  } else if (str.compare("VCS_GetSsiAbsEncoderParameterEx") == 0) {
    //VCS_GetSsiAbsEncoderParameterEx();
  } else if (str.compare("VCS_GetState") == 0) {
    unsigned short state;
    returnStatus = VCS_GetState(handle, node, &state, &error);
    if (state == ST_DISABLED)
      ack->response1 = "ST_DISABLED";
    else if(state == ST_ENABLED)
      ack->response1 = "ST_ENABLED";
    else if(state == ST_QUICKSTOP)
      ack->response1 = "ST_QUICKSTOP";
    else if(state == ST_FAULT)
      ack->response1 = "ST_FAULT";
    else
      ack->response1 = "UNKNOWN";
  } else if (str.compare("VCS_GetStepDirectionParameter") == 0) {
    //VCS_GetStepDirectionParameter();
  } else if (str.compare("VCS_GetTargetPosition") == 0) {
    long int position = 0;
    returnStatus = VCS_GetTargetPosition(handle, node, &position, &error);
    ack->response1 = std::to_string(position);
  } else if (str.compare("VCS_GetTargetVelocity") == 0) {
    //VCS_GetTargetVelocity();
  } else if (str.compare("VCS_GetVelocityIs") == 0) {
    int vel = 0;
    VCS_GetVelocityIs(handle, node, &vel, &error);
    ack->response1 = std::to_string(vel);
  } else if (str.compare("VCS_GetVelocityIsAveraged") == 0) {
    int vel = 0;
    VCS_GetVelocityIsAveraged(handle, node, &vel, &error);
    ack->response1 = std::to_string(vel);
  } else if (str.compare("VCS_GetVelocityMust") == 0) {
    //VCS_GetVelocityMust();
  } else if (str.compare("VCS_GetVelocityProfile") == 0) {
    //VCS_GetVelocityProfile();
  } else if (str.compare("VCS_GetVelocityRegulatorFeedForward") == 0) {
    //VCS_GetVelocityRegulatorFeedForward();
  } else if (str.compare("VCS_GetVelocityRegulatorGain") == 0) {
    //VCS_GetVelocityRegulatorGain();
  } else if (str.compare("VCS_GetVelocityUnits") == 0) {
    //VCS_GetVelocityUnits();
  } else if (str.compare("VCS_GetVersion") == 0) {
    //VCS_GetVersion();
  } else if (str.compare("VCS_HaltPositionMovement") == 0) {
    //VCS_HaltPositionMovement();
  } else if (str.compare("VCS_HaltVelocityMovement") == 0) {
    //VCS_HaltVelocityMovement();
  } else if (str.compare("VCS_MoveToPosition") == 0) {
    int absolute;
    if (a4.compare("True") == 0) {
      absolute = 1;
    } else if (a4.compare("False") == 0) {
      absolute = 0;
    } else {
      cout << "unknown absolute " << a4 << endl;
    }
    int immediate;
    if (a5.compare("True") == 0) {
      immediate = 1;
    } else if (a5.compare("False") == 0) {
      immediate = 0;
    } else {
      cout << "unknown immediate " << a5 << endl;
    }
    returnStatus = VCS_MoveToPosition(handle, node,
				      std::atoi(a3.c_str()),
				      absolute,
				      immediate,
				      &error);
  } else if (str.compare("VCS_MoveWithVelocity") == 0) {
    returnStatus = VCS_MoveWithVelocity((void *) handle,
					node,
					(short) std::atoi(a3.c_str()),
					&error);
  } else if (str.compare("VCS_OpenDevice") == 0) {
    
    handle = VCS_OpenDevice((char *) a1.c_str(),
			    (char *) a2.c_str(),
			    (char *) a3.c_str(),
			    (char *) a4.c_str(),
			    &error);
    returnStatus = *((int*) &handle);
  } else if (str.compare("VCS_OpenSubDevice") == 0) {
    handle = VCS_OpenSubDevice(handle,
			       (char *) a2.c_str(),
			       (char *) a3.c_str(),
			       &error);
    cout << "handle " << handle << endl;
    if (handle != 0)
      returnStatus = *((int*) &handle);
  } else if (str.compare("VCS_ReadCANFrame") == 0) {
    //VCS_ReadCANFrame();
  } else if (str.compare("VCS_ReadPositionMarkerCapturedPosition") == 0) {
    //VCS_ReadPositionMarkerCapturedPosition();
  } else if (str.compare("VCS_ReadPositionMarkerCounter") == 0) {
    //VCS_ReadPositionMarkerCounter();
  } else if (str.compare("VCS_RequestCANFrame") == 0) {
    //VCS_RequestCANFrame();
  } else if (str.compare("VCS_ResetDevice") == 0) {
    returnStatus = VCS_ResetDevice(handle, node, &error);
  } else if (str.compare("VCS_ResetPortNameSelection") == 0) {
    //VCS_ResetPortNameSelection();
  } else if (str.compare("VCS_ResetPositionMarkerCounter") == 0) {
    //VCS_ResetPositionMarkerCounter();
  } else if (str.compare("VCS_Restore") == 0) {
    //VCS_Restore();
  } else if (str.compare("VCS_SendCANFrame") == 0) {
    //VCS_SendCANFrame();
  } else if (str.compare("VCS_SendNMTService") == 0) {
    //VCS_SendNMTService();
  } else if (str.compare("VCS_SetAllDigitalOutputs") == 0) {
    //VCS_SetAllDigitalOutputs();
  } else if (str.compare("VCS_SetAnalogOutput") == 0) {
    //VCS_SetAnalogOutput();
  } else if (str.compare("VCS_SetAnalogOutputState") == 0) {
    //VCS_SetAnalogOutputState();
  } else if (str.compare("VCS_SetAnalogOutputVoltage") == 0) {
    //VCS_SetAnalogOutputVoltage();
  } else if (str.compare("VCS_SetControllerGain") == 0) {
    int regController;
    if (a3.compare("EC_PI_VELOCITY_CONTROLLER") == 0) {
      regController = EC_PI_VELOCITY_CONTROLLER;
    } else if (a3.compare("EC_PI_VELOCITY_CONTROLLER_WITH_OBSERVER") == 0) {
      regController = EC_PI_VELOCITY_CONTROLLER;
    } else {
      cout << "VCS_GetControllerGain arg2 unknown" << a3 << endl;
    }
    int regGain;
    if (a4.compare("EG_PIVC_P_GAIN") == 0) {
      regGain = EG_PIVC_P_GAIN;
    } else if (a4.compare("EG_PIVC_I_GAIN") == 0) {
      regGain = EG_PIVC_I_GAIN;
    } else if (a4.compare("EG_PIVC_FEED_FORWARD_VELOCITY_GAIN") == 0) {
      regGain = EG_PIVC_FEED_FORWARD_VELOCITY_GAIN;
    } else if (a4.compare("EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN") == 0) {
      regGain = EG_PIVC_FEED_FORWARD_ACCELERATION_GAIN;
    } else if (a4.compare("EG_PIVCWO_P_GAIN") == 0) {
      regGain = EG_PIVCWO_P_GAIN;
    } else if (a4.compare("EG_PIVCWO_I_GAIN") == 0) {
      regGain = EG_PIVCWO_I_GAIN;
    } else if (a4.compare("EG_PIVCWO_FEED_FORWARD_VELOCITY_GAIN") == 0) {
      regGain = EG_PIVCWO_FEED_FORWARD_VELOCITY_GAIN;
    } else if (a4.compare("EG_PIVCWO_FEED_FORWARD_ACCELERATION_GAIN") == 0) {
      regGain = EG_PIVCWO_FEED_FORWARD_ACCELERATION_GAIN;
    } else if (a4.compare("EG_PIVCWO_OBSERVER_THETA_GAIN") == 0) {
      regGain = EG_PIVCWO_OBSERVER_THETA_GAIN;
    } else if (a4.compare("EG_PIVCWO_OBSERVER_OMEGA_GAIN") == 0) {
      regGain = EG_PIVCWO_OBSERVER_OMEGA_GAIN;
    } else if (a4.compare("EG_PIVCWO_OBSERVER_TAU_GAIN") == 0) {
      regGain = EG_PIVCWO_OBSERVER_TAU_GAIN;
    } else {
      cout << "VCS_GetControllerGain arg4 unknown" << a4 << endl;
    }
    returnStatus = VCS_SetControllerGain(handle, node, regController, regGain, std::atoi(a5.c_str()), &error);
  } else if (str.compare("VCS_SetCurrentMust") == 0) {
    //VCS_SetCurrentMust();
  } else if (str.compare("VCS_SetCurrentRegulatorGain") == 0) {
    //VCS_SetCurrentRegulatorGain();
  } else if (str.compare("VCS_SetDcMotorParameter") == 0) {
    unsigned short nomCur = std::atoi(a3.c_str());
    unsigned short maxOutCur = std::atoi(a4.c_str());
    unsigned short thermalTimeConst = std::atoi(a5.c_str());
    returnStatus = VCS_SetDcMotorParameter(handle, node, nomCur, maxOutCur, thermalTimeConst, &error);
  } else if (str.compare("VCS_SetDisableState") == 0) {
    returnStatus = VCS_SetDisableState(handle, node, &error);
  } else if (str.compare("VCS_SetEcMotorParameter") == 0) {
    unsigned short nomCur = std::atoi(a3.c_str());
    unsigned short maxOutCur = std::atoi(a4.c_str());
    unsigned short thermalTimeConst = std::atoi(a5.c_str());
    unsigned char polePairs = std::atoi(a6.c_str());
    returnStatus = VCS_SetEcMotorParameter(handle, node, nomCur, maxOutCur, thermalTimeConst, polePairs, &error);
  } else if (str.compare("VCS_SetEnableState") == 0) {
    returnStatus = VCS_SetEnableState(handle, node, &error);
  } else if (str.compare("VCS_SetEncoderParameter") == 0) {
    //VCS_SetEncoderParameter();
  } else if (str.compare("VCS_SetGatewaySettings") == 0) {
    //VCS_SetGatewaySettings();
  } else if (str.compare("VCS_SetHallSensorParameter") == 0) {
    //VCS_SetHallSensorParameter();
  } else if (str.compare("VCS_SetHomingParameter") == 0) {
    returnStatus = VCS_SetHomingParameter(handle, node,
					  std::atoi(a3.c_str()),
					  std::atoi(a4.c_str()),
					  std::atoi(a5.c_str()),
					  std::atoi(a6.c_str()),
					  std::atoi(a7.c_str()),
					  std::atoi(a8.c_str()),
					  &error);
  } else if (str.compare("VCS_SetIncEncoderParameter") == 0) {
    //VCS_SetIncEncoderParameter();
  } else if (str.compare("VCS_SetIpmBufferParameter") == 0) {
    //VCS_SetIpmBufferParameter();
  } else if (str.compare("VCS_SetMasterEncoderParameter") == 0) {
    //VCS_SetMasterEncoderParameter();
  } else if (str.compare("VCS_SetMaxAcceleration") == 0) {
    returnStatus = VCS_SetMaxAcceleration(handle, node, std::atoi(a3.c_str()), &error);
  } else if (str.compare("VCS_SetMaxFollowingError") == 0) {
    //VCS_SetMaxFollowingError();
  } else if (str.compare("VCS_SetMaxProfileVelocity") == 0) {
    returnStatus = VCS_SetMaxProfileVelocity(handle, node, std::atoi(a3.c_str()), &error);
  } else if (str.compare("VCS_SetMotorParameter") == 0) {
    //VCS_SetMotorParameter();
  } else if (str.compare("VCS_SetMotorType") == 0) {
    unsigned short type = 0;
    if (a3.compare("MT_DC_MOTOR") == 0) {
      type = MT_DC_MOTOR;
    } else if (a3.compare("MT_EC_SINUS_COMMUTATED_MOTOR") == 0) {
      type = MT_EC_SINUS_COMMUTATED_MOTOR;
    } else if (a3.compare("MT_EC_BLOCK_COMMUTATED_MOTOR") == 0) {
      type = MT_EC_BLOCK_COMMUTATED_MOTOR;
    } else {
      cout << "VCS_SetMotorType arg2 unknown" << a3 << endl;
    }
    returnStatus = VCS_SetMotorType(handle, node, type, &error);
  } else if (str.compare("VCS_SetObject") == 0) {
    //VCS_SetObject();
  } else if (str.compare("VCS_SetOperationMode") == 0) {
    char mode;
    if (a3.compare("OMD_PROFILE_POSITION_MODE") == 0) {
      mode = OMD_PROFILE_POSITION_MODE;
    } else if (a3.compare("OMD_PROFILE_VELOCITY_MODE") == 0) {
      mode = OMD_PROFILE_VELOCITY_MODE;
    } else if (a3.compare("OMD_HOMING_MODE") == 0) {
      mode = OMD_HOMING_MODE;
    } else if (a3.compare("OMD_INTERPOLATED_POSITION_MODE") == 0) {
      mode = OMD_INTERPOLATED_POSITION_MODE;
    } else if (a3.compare("OMD_POSITION_MODE") == 0) {
      mode = OMD_POSITION_MODE;
    } else if (a3.compare("OMD_VELOCITY_MODE") == 0) {
      mode = OMD_VELOCITY_MODE;
    } else if (a3.compare("OMD_CURRENT_MODE") == 0) {
      mode = OMD_CURRENT_MODE;
    } else if (a3.compare("OMD_MASTER_ENCODER_MODE") == 0) {
      mode = OMD_MASTER_ENCODER_MODE;
    } else if (a3.compare("OMD_STEP_DIRECTION_MODE") == 0) {
      mode = OMD_STEP_DIRECTION_MODE;
    } else {
      cout << "unknown mode " << a3 << endl;
    } 
    returnStatus = VCS_SetOperationMode(handle, node, mode, &error);
  } else if (str.compare("VCS_SetPositionCompareParameter") == 0) {
    //VCS_SetPositionCompareParameter();
  } else if (str.compare("VCS_SetPositionCompareReferencePosition") == 0) {
    //VCS_SetPositionCompareReferencePosition();
  } else if (str.compare("VCS_SetPositionMarkerParameter") == 0) {
    //VCS_SetPositionMarkerParameter();
  } else if (str.compare("VCS_SetPositionMust") == 0) {
    //VCS_SetPositionMust();
  } else if (str.compare("VCS_SetPositionProfile") == 0) {
    returnStatus = VCS_SetPositionProfile(handle, node,
					  std::atoi(a3.c_str()),
					  std::atoi(a4.c_str()),
					  std::atoi(a5.c_str()),
					  &error);
  } else if (str.compare("VCS_SetPositionRegulatorFeedForward") == 0) {
    //VCS_SetPositionRegulatorFeedForward();
  } else if (str.compare("VCS_SetPositionRegulatorGain") == 0) {
    //VCS_SetPositionRegulatorGain();
  } else if (str.compare("VCS_SetProtocolStackSettings") == 0) {
    returnStatus = VCS_SetProtocolStackSettings(handle,
						std::atoi(a2.c_str()),
						std::atoi(a3.c_str()),
						&error);
  } else if (str.compare("VCS_SetQuickStopState") == 0) {
    //VCS_SetQuickStopState();
  } else if (str.compare("VCS_SetSensorType") == 0) {
    unsigned short st = 0;
    if (a3.compare("ST_UNKNOWN") == 0) {
      st = ST_UNKNOWN;
    } else if (a3.compare("ST_INC_ENCODER_3CHANNEL") == 0) {
      st = ST_INC_ENCODER_3CHANNEL;
    } else if (a3.compare("ST_INC_ENCODER_2CHANNEL") == 0) {
      st = ST_INC_ENCODER_2CHANNEL;
    } else if (a3.compare("ST_HALL_SENSORS") == 0) {
      st = ST_HALL_SENSORS;
    } else if (a3.compare("ST_SSI_ABS_ENCODER_BINARY") == 0) {
      st = ST_SSI_ABS_ENCODER_BINARY;
    } else if (a3.compare("ST_SSI_ABS_ENCODER_GREY") == 0) {
      st = ST_SSI_ABS_ENCODER_GREY;
    } else if (a3.compare("ST_INC_ENCODER2_3CHANNEL") == 0) {
      st = ST_INC_ENCODER2_3CHANNEL;
    } else if (a3.compare("ST_INC_ENCODER2_2CHANNEL") == 0) {
      st = ST_INC_ENCODER2_2CHANNEL;
    } else if (a3.compare("ST_ANALOG_INC_ENCODER_3CHANNEL") == 0) {
      st = ST_ANALOG_INC_ENCODER_3CHANNEL;
    } else if (a3.compare("ST_ANALOG_INC_ENCODER_2CHANNEL") == 0) {
      st = ST_ANALOG_INC_ENCODER_2CHANNEL;
    } else {
      cout << "VCS_SetSensorType arg3 unknown " << a3 << endl;
    }
    returnStatus = VCS_SetSensorType(handle, node, st, &error);
  } else if (str.compare("VCS_SetSsiAbsEncoderParameter") == 0) {
    //VCS_SetSsiAbsEncoderParameter();
  } else if (str.compare("VCS_SetSsiAbsEncoderParameterEx") == 0) {
    //VCS_SetSsiAbsEncoderParameterEx();
  } else if (str.compare("VCS_SetState") == 0) {
    //VCS_SetState();
  } else if (str.compare("VCS_SetStepDirectionParameter") == 0) {
    //VCS_SetStepDirectionParameter();
  } else if (str.compare("VCS_SetVelocityMust") == 0) {
    //VCS_SetVelocityMust();
  } else if (str.compare("VCS_SetVelocityProfile") == 0) {
    //VCS_SetVelocityProfile();
  } else if (str.compare("VCS_SetVelocityRegulatorFeedForward") == 0) {
    //VCS_SetVelocityRegulatorFeedForward();
  } else if (str.compare("VCS_SetVelocityRegulatorGain") == 0) {
    //VCS_SetVelocityRegulatorGain();
  } else if (str.compare("VCS_SetVelocityUnits") == 0) {
    //VCS_SetVelocityUnits();
  } else if (str.compare("VCS_StartIpmTrajectory") == 0) {
    //VCS_StartIpmTrajectory();
  } else if (str.compare("VCS_StopHoming") == 0) {
    //VCS_StopHoming();
  } else if (str.compare("VCS_StopIpmTrajectory") == 0) {
    //VCS_StopIpmTrajectory();
  } else if (str.compare("VCS_Store") == 0) {
    //VCS_Store();
  } else if (str.compare("VCS_WaitForHomingAttained") == 0) {
    //VCS_WaitForHomingAttained();
  } else if (str.compare("VCS_WaitForTargetReache") == 0) {
    //VCS_WaitForTargetReache();
  } else {
    cout << "Unknown VCS command received " << str;
  }
  if (returnStatus != 1)
    cout << "error 0x" << hex << error << endl;
  ack->status = std::to_string(returnStatus);
  ack->error = std::to_string(error);
  return ack;
}

