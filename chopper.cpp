#include <iostream>
#include "Definitions.h"
#include <string.h>
#include <sstream>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <list>
#include <math.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/times.h>
#include <sys/time.h>

#include "server.h"
#include "vcs_api.h"

#ifndef MMC_SUCCESS
#define MMC_SUCCESS 0
#endif

#ifndef MMC_FAILED
#define MMC_FAILED 1
#endif

using namespace std;

void* key = 0;
unsigned short node = 1;
string g_deviceName;
string g_protocolStackName;
string g_interfaceName;
string g_portName;
int g_baudrate = 0;
unsigned int ulErrorCode = 0;
bool oIsEnabled = 0;
int oIsFault = 0;

void LogInfo(string message)
{
  cout << message << endl;
}

void LogError(string functionName, int p_lResult, unsigned int p_ulErrorCode)
{
  cerr << functionName << " failed (result=" << p_lResult << ", errorCode=0x" << std::hex << p_ulErrorCode << ")"<< endl;
}

void SetDefaultParameters()
{
  //USB
  node = 1;
  g_deviceName = "EPOS4"; 
  g_protocolStackName = "MAXON SERIAL V2"; 
  g_interfaceName = "USB"; 
  g_portName = "USB0"; 
  g_baudrate = 1000000; 
}

void PrintSettings()
{
  stringstream msg;

  msg << "default settings:" << endl;
  msg << "node id             = " << node << endl;
  msg << "device name         = '" << g_deviceName << "'" << endl;
  msg << "protocal stack name = '" << g_protocolStackName << "'" << endl;
  msg << "interface name      = '" << g_interfaceName << "'" << endl;
  msg << "port name           = '" << g_portName << "'"<< endl;
  msg << "baudrate            = " << g_baudrate;

  LogInfo(msg.str());
}

void clearFaults() {

  unsigned int p_pErrorCode;
  int lResult = MMC_FAILED;
  
  cout << "VCS_GetFaultState" << endl;
  cout << "VCS_GetFaultState key " << key << " node " << node << endl;
  if(VCS_GetFaultState(key, node, &oIsFault, &p_pErrorCode ) == 0) {
    LogError("VCS_GetFaultState", lResult, p_pErrorCode);
  }

  if(lResult==0)
    {
      if(oIsFault)
	{
	  stringstream msg;
	  msg << "clear fault, node = '" << node << "'";
	  LogInfo(msg.str());

	  cout << "VCS_ClearFault" << endl;
	  if(VCS_ClearFault(key, node, &p_pErrorCode) == 0)
	    {
	      LogError("VCS_ClearFault", lResult, p_pErrorCode);
	    }
	}

      if(lResult==0)
	{
	  int oIsEnabled = 0;

	  cout << "VCS_GetEnableState" << endl;
	  if(VCS_GetEnableState(key, node, &oIsEnabled, &p_pErrorCode) == 0)
	    {
	      LogError("VCS_GetEnableState", lResult, p_pErrorCode);
	    }

	  if(lResult==0)
	    {
	      if(!oIsEnabled)
		{
		  cout << "VCS_SetEnableState" << endl;
		  if(VCS_SetEnableState(key, node, &p_pErrorCode) == 0)
		    {
		      LogError("VCS_SetEnableState", lResult, p_pErrorCode);
		    }
		}
	    }
	}
    }
  return;
}

int OpenDevice(unsigned int* p_pErrorCode)
{
  int lResult = MMC_FAILED;

  char* pDeviceName = new char[255];
  char* pProtocolStackName = new char[255];
  char* pInterfaceName = new char[255];
  char* pPortName = new char[255];

  strcpy(pDeviceName, g_deviceName.c_str());
  strcpy(pProtocolStackName, g_protocolStackName.c_str());
  strcpy(pInterfaceName, g_interfaceName.c_str());
  strcpy(pPortName, g_portName.c_str());

  LogInfo("Open device...");

  cout << "VCS_OpenDevice" << endl;
  key = VCS_OpenDevice(pDeviceName, pProtocolStackName, pInterfaceName, pPortName, p_pErrorCode);

  if(key!=0 && *p_pErrorCode == 0)
    {
      unsigned int lBaudrate = 0;
      unsigned int lTimeout = 0;

      cout << "VCS_GetProtocolStackSettings" << endl;
      if(VCS_GetProtocolStackSettings(key, &lBaudrate, &lTimeout, p_pErrorCode)!=0)
	{
	  cout << "VCS_SetProtocolStackSettings" << endl;
	  if(VCS_SetProtocolStackSettings(key, g_baudrate, lTimeout, p_pErrorCode)!=0)
	    {
	      cout << "VCS_GetProtocolStackSettings" << endl;
	      if(VCS_GetProtocolStackSettings(key, &lBaudrate, &lTimeout, p_pErrorCode)!=0)
		{
		  if(g_baudrate==(int)lBaudrate)
		    {
		      lResult = MMC_SUCCESS;
		    }
		}
	    }
	}
    }
  else
    {
      key = 0;
    }

  delete []pDeviceName;
  delete []pProtocolStackName;
  delete []pInterfaceName;
  delete []pPortName;

  return lResult;
}

int CloseDevice(unsigned int* p_pErrorCode)
{
  int lResult = MMC_FAILED;

  *p_pErrorCode = 0;

  LogInfo("Close device");

  cout << "VCS_CloseDevice" << endl;
  if(VCS_CloseDevice(key, p_pErrorCode)!=0 && *p_pErrorCode == 0)
    {
      lResult = MMC_SUCCESS;
    }

  return lResult;
}

int main(int argc, char** argv)
{
  struct timespec start, finish;
  double elapsed;
  int lResult;
  unsigned int lErrorCode = 0;
  bool test = false;

  if (test) {
    SetDefaultParameters();
    PrintSettings();
    if((lResult = OpenDevice(&ulErrorCode))!= MMC_SUCCESS) {
      LogError("OpenDevice", lResult, ulErrorCode);
      return lResult;
    }
    clearFaults();

    cout << "VCS_SetEnableState" << endl;
    if(VCS_SetEnableState(key, node, &lErrorCode) == 0) {
      lResult = MMC_FAILED;
      LogError("VCS_SetEnableState", lResult, lErrorCode);
    }

    cout << "VCS_ActivateProfileVelocityMode" << endl;
    if(VCS_ActivateProfileVelocityMode(key, node, &lErrorCode) == 0) {
      LogError("VCS_ActivateProfileVelocityMode", lResult, lErrorCode);
      lResult = MMC_FAILED;
    }
  
    long targetVelocity = 250;
    cout << "VCS_MoveWithVelocity" << endl;
    if(VCS_MoveWithVelocity(key, node, targetVelocity, &lErrorCode) == 0) {
      lResult = MMC_FAILED;
      LogError("VCS_MoveWithVelocity", lResult, lErrorCode);
    }
    unsigned char dimension;
    char notation[256];
    cout << "VCS_GetVelocityUnits" << endl;
    if (VCS_GetVelocityUnits(key, node, &dimension, notation, &lErrorCode) == 0) {
      lResult = MMC_FAILED;
      LogError("VCS_MoveWithVelocity", lResult, lErrorCode);
    }

    clock_gettime(CLOCK_MONOTONIC, &start);
    do {
      int velocity;
      short current;
      cout << "VCS_GetVelocityIs" << endl;
      if (VCS_GetVelocityIs(key, node, &velocity, &lErrorCode) == 0) {
	lResult = MMC_FAILED;
	LogError("VCS_GetVelocityIs", lResult, lErrorCode);
      }
      cout << "VCS_GetCurrentIs" << endl;
      if (VCS_GetCurrentIs(key, node, &current, &lErrorCode) == 0) {
	lResult = MMC_FAILED;
	LogError("VCS_GetCurrentIs", lResult, lErrorCode);
      }
      stringstream msg;
      //msg << "velocity " << velocity << " current " << current << endl;
      msg << "current " << current / 1000.0;
      //LogInfo(msg.str());
      cout << "VCS_GetVelocityIsAveraged" << endl;
      if (VCS_GetVelocityIsAveraged(key, node, &velocity, &lErrorCode) == 0) {
	lResult = MMC_FAILED;
	LogError("VCS_GetVelocityIsAveraged", lResult, lErrorCode);
      }
      cout << "VCS_GetCurrentIsAveraged" << endl;
      if (VCS_GetCurrentIsAveraged(key, node, &current, &lErrorCode) == 0) {
	lResult = MMC_FAILED;
	LogError("VCS_MoveWithVelocity", lResult, lErrorCode);
      }
      //msg.str("");
      msg << " velocityAveraged " << velocity << " currentAveraged " << current / 1000.0;
      LogInfo(msg.str());
      usleep(250000);
      clock_gettime(CLOCK_MONOTONIC, &finish);
      elapsed = finish.tv_sec - start.tv_sec;
      elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    } while (elapsed < 2.0);
  
    cout << "VCS_SetDisableState" << endl;
    if(VCS_SetDisableState(key, node, &lErrorCode) == 0) {
      LogError("VCS_SetDisableState", lResult, lErrorCode);
      lResult = MMC_FAILED;
    }
    if((lResult = CloseDevice(&ulErrorCode))!=MMC_SUCCESS) {
      LogError("CloseDevice", lResult, ulErrorCode);
      return lResult;
    }
  }

  startServer();

  while(true) { sleep(1); }
  
  killServer();

}

